# ISPConfig plugin

## Application informations 
This application is an Add-ons for ISPConfig used to restrict email creation based on pattern match

## Requirements
- ISPConfig 3.1+
- If you wan't that plugin working with the API you have to do a little modification inside the remoce classes : https://git.ispconfig.org/ispconfig/ispconfig3/-/issues/6032


## Features 
When you add, edit a mailbox the plugin do 
- Check the email address and if we habe a match
  - Block the insertion / modification
  - Return an error 
  - Add an entry inside the error.log


## How To implement this module
- Deploy this plugin into -> /usr/local/ispconfig/interface/lib/plugins
- Configure the plugin -> /usr/local/ispconfig/interface/lib/config.inc.local.php
  - $conf['username_pattern_blacklist_plugin'] = ['.*pattern1.*','.*patt.*ern2.*'];
 
## ToDo
- Extend the check to the alias and forward

# License
Copyright (c) 2021, Oricom Internet
All rights reserved.
