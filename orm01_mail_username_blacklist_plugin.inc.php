<?php
/**
 * orm01_mail_username_blacklist_plugin plugin 
 *
 * @author:  LEVEILLE Cédric (leveille.cedric@oricom.org) -  Oricom Internet Inc.
 * Requirement :
 * - add an array with your blacklisted pattern inside the config file of ISPConfig config.inc.local.php
 * 		Ex: $conf['username_pattern_blacklist_plugin'] = ['.*pattern1.*','.*patt.*ern2.*']
 **/

class orm01_mail_username_blacklist_plugin {

	var $plugin_name = 'orm01_mail_username_blacklist_plugin';
	var $class_name = 'orm01_mail_username_blacklist_plugin';

	const DEBUG = FALSE;

	function onLoad() {
		global $app;
		
		$app->plugin->registerEvent('mail:mail_user:on_after_insert', 'orm01_mail_username_blacklist_plugin', 'after_user_insert');
		$app->plugin->registerEvent('mail:mail_user:on_after_update', 'orm01_mail_username_blacklist_plugin', 'after_user_update');
	}

	function after_user_insert($event_name, $page_form){
		global $app, $conf;	

		if (self::DEBUG == true ){error_log('AFTER INSERT :: ' . json_encode( $page_form )) ; }
		
		// IF : Low privileges user or API call
		if( $_SESSION["s"]["user"]["typ"] != 'admin'  || $_SESSION["client_login"] == 0 ) 
		{ 
			if (isset($conf['username_pattern_blacklist_plugin']))
			{
				foreach ($conf['username_pattern_blacklist_plugin'] as $pattern)
				{
					if(preg_match("/{$pattern}/", $page_form->dataRecord['email']))
					{
						error_log("MATCH FOUND :: ". $pattern . " -> Automatic email deletion ");
						$app->db->query("DELETE FROM mail_user WHERE mailuser_id = ?", $page_form->id);
											
						if ($_SESSION["s"]["user"]["typ"] != 'admin')
						{
							$app->error('Opération non permise / Operation not allowed', '', true, 1);
							
						}
						else // Output for API
						{
							$error = json_encode([
								"code" => "remote_fault", 
								"message"=> "Operation not allowed",
								"response" => false,
							]);
							die($error);
						}
					}
				}
			} 	
		} 
	}
	
	function after_user_update($event_name, $page_form){
		global $app, $conf;	

		if (self::DEBUG == true ){error_log('AFTER UPDATE :: ' . json_encode( $page_form )) ; }	
		
		// IF : Low privileges user or API call
		if( $_SESSION["s"]["user"]["typ"] != 'admin' || $_SESSION["client_login"] == 0 )
		{ 
			if (isset($conf['username_pattern_blacklist_plugin']))
			{
				foreach ($conf['username_pattern_blacklist_plugin'] as $pattern)
				{
					if(preg_match("/{$pattern}/", $page_form->dataRecord['email']))
					{
						error_log("MATCH FOUND :: ". $pattern . " -> Settings rollback");
						$app->db->query("UPDATE mail_user SET email = ?, login = ?, maildir = ?  WHERE mailuser_id = ?", $page_form->oldDataRecord['email'],  $page_form->oldDataRecord['login'], $page_form->oldDataRecord['maildir'] , $page_form->id);
						
						if ($_SESSION["s"]["user"]["typ"] != 'admin')
						{
							$app->error('Opération non permise / Operation not allowed', '', true, 1);
							
						}
						else // Output for API
						{
							$error = json_encode([
								"code" => "remote_fault", 
								"message"=> "Operation not allowed",
								"response" => false,
							]);
							die($error);
						}
					}
				} 
			}
		}
	}

}	 // End class
